
var a = [0,10,20,30,40,50,60,70,80,90,40];

/*
// Ponteiros
alert(a);
var b = a;
b[0] = 0;
alert(a);
*/

/*
for(var i=0;i<a.length;i++) {
	console.log(a[i]);
}

a.forEach(function(elem,index) {
	console.log("a["+index+"] = "+elem);
});
*/



// Map executa a operacao em todos elementos do vetor
/*
alert(a);
var c = a.map(function(x){
	return x*x;
});
alert(c);
*/


// Reduz o vetor a um valor executando a operacao definida pela funcao
/*
alert(a);
var c = a.reduce(function(valorAnterior, valorAtual, indice, array){
	return valorAnterior+valorAtual;
});
alert(c);
*/

// Filtro os valores do vetor
/*
alert(a);
var c = a.filter(function(x){
	return x <= 7;	
});
alert(c);
*/

// Concatena vetores
/*
var d = ["asa","ksks","popop"];
var e = [64,65]
alert(a);
var c = a.concat(d,e);
alert(c);
*/


// Retorna o vetor como strind separado pelo caracter escolhido
/*
alert(a.join("<----->"));
*/

// ==== Strings ====

// divide a string em vetores 
/*
var c = "aq be cr";
var d = c.split(" ",1);
alert(d[0]);
alert(d.length);
*/

// Susbtitui string dentro de outra string
var c = "Oi Pedro!";
alert(c.replace("Pedro","Edu"));

// e muito mais ...



