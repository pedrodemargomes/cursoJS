// Sem jquery
//var quadrado = document.getElementById("quadrado");
//quadrado.style.backgroundColor = "blue";

// Com jquery
//var quadrado = $("#quadrado");
//quadrado.css('background-color','blue');

// $("#nomeDoId") Retorna primeiro objeto com id=nomeDoId
// $("nomeDaTag") Retorna lista de objetos com tag=nomeDaTag
// $(".nomeDaClasse") Retorna lista de objetos com class=nomeDaClasse
// $("#nomeDoId1 #nomeDoId2 .nomeDaClasse1 .nomeDaClasse2 nomeDaTag1 . . .")

/*
$('#quadrado').css('background-color','blue');

alert($('#quadrado').css('background-color'));
*/

/*
// ==== click ========
// ==== mouseenter ===
// ==== mouseleave ===
// ==== dblclick =====

function acaoCor() {
	// $('#quadrado').css('background-color','blue');
	// ou 
	$('#quadrado').css({'background-color': 'blue'});
}

function acaoMove() {
	if( $('#quadrado').css("width") == "50px" ) {
		$('#quadrado').animate({'width':'500px',"height":"500px"},500);
	} else {
		$('#quadrado').animate({'width':'50px',"height":"50px"},500);
	}
}

$('#quadrado').bind("click",acaoCor);
*/


















