var objeto = { nome: "Maria",
			idade: 20,
			curso: "bcc",
			mudaNome: function(nome) {
				this.nome = nome;	
			},
			mudaCurso: function(curso) {
				this.curso = curso;
			}
		};

objeto.mudaNome("Joao");
objeto.mudaCurso("tads");
objeto.cpf = "32465484210";

objeto.imprimeInfo = function() {
	alert(objeto.nome+"\n"+objeto.idade+"\n"+objeto.curso+"\n"+objeto.cpf);
}


objeto.imprimeInfo();

